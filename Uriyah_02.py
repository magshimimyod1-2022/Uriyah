import socket
from datetime import datetime, timedelta

SERVER_IP = "34.218.16.79"
SERVER_PORT = 77
DIFFERENCE_IN_ASCII_CODES = 96
TEMP_LENGTH = 5
WINDOWS_LIMIT = 1024
END_OF_WEATHER_MSG = 6
REMOVE_ERROR_WARNING = 10

def weather_client(city, date):
    """
    function that sends a request to the server and returns the weather
    input: city, date
    output: res_tup or error message
    """
    msg = ""
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (SERVER_IP, SERVER_PORT)
    sock.connect(server_address)
    # recieve the welcome message from the server
    server_msg = sock.recv(WINDOWS_LIMIT)
    server_msg = server_msg.decode()
    msg = "100:REQUEST:city={}&date={}&checksum={}".format(city, date, calc_checksum(city, date)) 
    sock.sendall(msg.encode())
    server_msg = sock.recv(WINDOWS_LIMIT)
    server_msg = server_msg.decode()
    if server_msg[:3] == "200": #if the server sends a response
        temp_start = server_msg.find("temp=") + TEMP_LENGTH #finds the start of the temperature in the response
        temp_end = server_msg.find("&text=") #finds the end of the temperature in the response
        temperature = (server_msg[temp_start:temp_end])
        weather_type = server_msg[temp_end + END_OF_WEATHER_MSG:] #finds the weather type in the response
        res_tup = (temperature, weather_type)
    elif server_msg[:3] == "500": #if the server sends an error
        res_tup = ("999", server_msg[REMOVE_ERROR_WARNING:]) 
    return res_tup

def todays_date():
    time = datetime.today().strftime(format='%d/%m/%Y')
    return str(time)

def todays_and_three_days_ahead(city):
    """
    function that calculates the weather for today and the next three days
    input: city
    output: none
    """
    # finds today's date, and formats it to DD/MM/YYYY
    time = datetime.today().strftime(format='%d/%m/%Y')
    for i in range(4):
        time = datetime.today() + timedelta(days=i)
        time = time.strftime(format='%d/%m/%Y')
        res = weather_client(city, time)
        if "999" in res: #if there is an error 
            print(res[0] + ": " + res[1])
        else:
            print(time + ", Temperature: " + res[0] + ", " + res[1] + ".")
            

def calc_checksum(city, date):
    """
    function that calculates the checksum for the request
    input: city, date
    output: checksum
    """
    checksum1 = 0 #checksum of city sum.
    checksum2 = 0 #checksum of sum of digits of date
    for letter in city:
        checksum1 += ord(letter.lower()) - DIFFERENCE_IN_ASCII_CODES #calculates checksum of city
    date = date.split("/")
    date = "".join(date)
    for num in date:
        checksum2 += int(num) #calculates checksum of sum of digits of date
    return str(checksum1) + "." + str(checksum2)
    
def main():
    country_and_city = input("Enter the country and city that you live in (France, Paris): ")
    option = int(input("Enter 1 for today's weather or 2 for today's and 3 days ahead: "))
    city, country = country_and_city.split(", ")
    date = todays_date()
    if option == 1:
        res = weather_client(city , date)
        if "999" in res: #if there is an error
            print(res[0] + ": " + res[1])
        else:        
            print(date + ", Temperature: " + res[0] + ", " + res[1] + ".") 
    elif option == 2:
        todays_and_three_days_ahead(city)

        
if __name__ == "__main__":
    main()
