def print_screen():
    print("""  _    _                                         
    | |  | |                                        
    | |__| | __ _ _ __   __ _ _ __ ___   __ _ _ __  
    |  __  |/ _` | '_ \ / _` | '_ ` _ \ / _` | '_ \ 
    | |  | | (_| | | | | (_| | | | | | | (_| | | | |
    |_|  |_|\__,_|_| |_|\__, |_| |_| |_|\__,_|_| |_|
                        __/ |                      
                        |___/""")
                        
def choose_word(file_path, index):
    '''
    :param file_path: path to a file with a word
    :type arg1:  string
    :param index: index of word in file
    :type arg2:  int
    :return: word
    :rtype: string
    '''
    input_file = open(file_path, 'r')
    line = input_file.read()
    line = line.split(' ')
    input_file.close()
    index = index % len(line)
    line_set = set(line)
    num_line = len(line_set)
    tup = tuple((num_line, line[index-1]))
    return tup

def try_update_letter_guessed(letter_guessed, old_letters_guessed):
    """
    param: letter_guessed: string, the letter the user is guessing
    param: old_letters_guessed: list (of letters), which letters have been guessed so far
    return: boolean, True if the letter is in the word to guess
    """
    ret_val = check_valid_input(letter_guessed, old_letters_guessed)
    if(ret_val == True):
        old_letters_guessed.append(letter_guessed.lower())
    else:
        sorted_list = sorted(old_letters_guessed)
        sorted_list = " -> ".join(sorted_list)
        print("X")
        print(sorted_list)
    return ret_val

def check_win(secret_word, old_letters_guessed):
    '''
    secret_word: string, the word the user is guessing
    old_letters_guessed: list (of letters), which letters have been guessed so far
    returns: boolean, True if all the letters of secret_word are in old_letters_guessed;
      False otherwise
    '''
    for char in secret_word:
        if char not in old_letters_guessed:
            return False
    return True

def check_valid_input(letter_guessed, old_letters_guessed):
    """
    function to check if the input is a valid letter
    input: letter_guessed, old_letters_guessed
    output: ret_val
    """
    ret_val = False
    if(letter_guessed.lower() not in old_letters_guessed):
        if(letter_guessed.upper() not in old_letters_guessed):
            if(len(letter_guessed) == 1):
                if(letter_guessed.isalpha()):
                    ret_val = True
    return ret_val

def show_hidden_word(secret_word, old_letters_guessed):
    '''
    secret_word: string, the word the user is guessing
    old_letters_guessed: list (of letters), which letters have been guessed so far
    returns: string, comprised of letters, underscores (_), and spaces that represents
      which letters in secret_word have been guessed so far.
    '''
    result = ''
    for char in secret_word:
        if char in old_letters_guessed:
            result += char
            result += ' '
        else:
            result += '_ '
    return result.strip()

def print_hangman(num_of_tries):
    tries = {
        "1": "x-------x",
        "2": """    x-------x
    |
    |
    |
    |
    |""",
        "3": """    x-------x
    |       |
    |       0
    |
    |
    |""",
        "4": """    x-------x
    |       |
    |       0
    |       |
    |
    |""",
        "5": """    x-------x
    |       |
    |       0
    |      /|\\
    |
    |""",
        "6": """    x-------x
    |       |
    |       0
    |      /|\\
    |      /
    |""",
        "7": """    x-------x
    |       |
    |       0
    |      /|\\
    |      / \\
    |"""
    }
    if num_of_tries == 1:
        print(tries["1"])
    elif num_of_tries == 2:
        print(tries["2"])

    elif num_of_tries == 3:
        print(tries["3"])

    elif num_of_tries == 4:
        print(tries["4"])

    elif num_of_tries == 5:
        print(tries["5"])

    elif num_of_tries == 6:
        print(tries["6"])

    elif num_of_tries == 7:
        print(tries["7"])
        
def main():
    win = False
    num_of_tries = 1
    old_letters_guessed = []
    print_screen()
    file_path = input("Enter the file path: ")
    index = int(input("Enter the index of the word: "))
    print("Let's Start!")
    word = choose_word(file_path, index)
    secret_word = word[1]
    while num_of_tries <= 6 and win == False:
        print(show_hidden_word(secret_word, old_letters_guessed))
        letter_guessed = input("Guess a letter: ")
        while (try_update_letter_guessed(letter_guessed, old_letters_guessed) == False):
            letter_guessed = input("Guess a letter: ")
        if(letter_guessed.lower() in secret_word):
            if(check_win(secret_word, old_letters_guessed)):
                print(show_hidden_word(secret_word, old_letters_guessed))
                print("WIN")
                win = True
        else:
            print(":(")
            num_of_tries += 1
            print_hangman(num_of_tries)
    if(num_of_tries > 6):
        print(show_hidden_word(secret_word, old_letters_guessed))
        print("LOSE")

if __name__ == "__main__":
    main()