import requests
from collections import Counter

def extract_password_from_site():
    """
    :function to extract the password from the site
    :return password
    """
    URL_root = "http://webisfun.cyber.org.il/nahman/files/file"
    passwd = ""
    for i in range(11,35):
        URL = URL_root + str(i) + ".nfo"
        response = requests.get(URL)
        passwd += response.text[99]
    return passwd


def find_most_common_words(file_path, num_of_words):
    """
    :function to find the most common words in the file
    :param file_path:
    :param num_of_words:
    :return string of the most common words
    """
    count = {}
    my_str = ""
    open_file = open(file_path, "r")
    read_file = open_file.read()
    split_words = read_file.split()
    count = Counter(split_words)
    most_occur = count.most_common(num_of_words)
    for item in most_occur:
        my_str += item[0] + " "
    open_file.close()
    return my_str


def main():
    option = int(input("Choose an option, 1: extract password from site, 2: find most common words in file: "))
    if option == 1:
        print(extract_password_from_site())
    elif option == 2:
        file_path ="words.txt"
        num_of_words = 6
        print(find_most_common_words(file_path, num_of_words))

if __name__ == "__main__":
    main()